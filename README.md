# Périmètre des modules :

## Gestions utilisateurs :
	- Création d'un utilisateur :
		- nom *
		- prénom *
		- photo
		- email *
		- password *

## Gestions Team :
	- Création de team : ( je rajoute ça pour voir après pour le proposer a d'autre team, nottament aux frogs ;) )
		- nom *
		- lieu *
		- logo

	- Création des responsables/référents:
		- user
		- poste * ( prez, trésorier, responsable com ...  )

## Gestions sections :
	- Création sections : 
		- nom *
		- limite d'âge *
		- coach principal *
		- team * 
		- aides ( ex: Jérôme/Michel softball )
		- jours d'entrainements prévus ( ex: senior : lundi et mercredi )
		- période entrainements : ( ex: du 8 sept 2014 au 30 juin 2015 )

	ajout de joueur :
		- user *
		- équipe *
		- N° eRoster
	autres données :
		- statut ( ex: blessé à la jambe, déménagement ... )



## Gestions des events :
	- Création d'un event :
		- équipes concernés
		- date et heure du rdv
		- lieu du rdv
		- 

## Gestions des matchs :
	- Création de match :
		- équipe home * ( création de fiche team OTF )
		- équipe visitor * ( création de fiche team OTF )
		- lieu de match ( création de fiche team OTF )
		- date et heure du playball
		- date et heure de l'entrainement
		- date et heure du rdv matos
		- arbitre ( possibilité plusieurs )
		- scoreur

## Gestions des lieues :
	- Création d'un lieu :
		- nom du lieu *
		- type de terrain *
		- adresse ( cp, rue, ville, gmap) *
		- ville *
		- type de sol
		- chaussure autorisés
		- team ( création de fiche team OTF )

## fonctionnalités attendues
	- Mails envoyé par le coach aux selectionés du matchs.
	- Mails de relance pour ceux qui n'ont pas répondus a un event.
	- Mails de relance pour ceux qui ne ce sont pas décidé pour l'event ( "je sais pas" / "?" )

* champs demandés à la création
 No newline at end of file
