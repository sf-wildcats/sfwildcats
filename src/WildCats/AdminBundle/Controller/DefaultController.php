<?php

namespace WildCats\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $content = $this->renderView(
            'WildCatsAdminBundle:Default:index.html.twig'
        );
        
        return new Response($content);
    }
    
    /**
     * @Route("/admin/example")
     * @Template()
     */
    public function exampleAction()
    {
        $content = $this->renderView(
            'WildCatsAdminBundle:Default:example.html.twig'
        );
        
        return new Response($content);
    }
}
