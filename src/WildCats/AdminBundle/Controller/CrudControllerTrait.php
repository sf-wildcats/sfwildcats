<?php

namespace WildCats\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

trait CrudControllerTrait {

    protected $listFields = array('id');

    /**
     * Displays a form to create a new document.
     *
     * @return array
     */
	public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository($this->getCurrentBundleName() . ':' . ucfirst($this->documentName))->findAll();

        return $this->render('WildCatsAdminBundle:Crud:list.html.twig', 
            array(
                'document_name' => strtolower($this->documentName),
                'documents' => $documents,
                'list' => $this->setListFields()
            )
        );
    }

    /**
     * Displays a information for a document.
     *
     * @return array
     */
    public function showAction($id) {

        $dm = $this->getDocumentManager();
        $document = $dm->getRepository($this->getCurrentBundleName() . ':' . ucfirst($this->documentName))->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find User document.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('WildCatsAdminBundle:Crud:show.html.twig', 
            array(
                'document_name' => strtolower($this->documentName),
                'document' => $document,
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Displays a form to create a new document.
     *
     * @return array
     */
    public function newAction()
    {
        $document = new $this->documentClass('');
        $form = $this->createForm(new $this->documentType, $document);

        return $this->render('WildCatsAdminBundle:Crud:new.html.twig', 
            array(
                'document_name' => strtolower($this->documentName),
                'action' => 'new',
                'document' => $document,
                'form' => $form->createView()
            )
        );
    }

    /**
     * Creates a new User document.
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $document = new User();
        $form     = $this->createForm(new UserType(), $document);
        $form->bind($request);

        if ($form->isValid()) {

            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_' . $this->documentName . '_edit', array('id' => $document->getId())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Displays a Form for edit a document.
     *
     * @return array
     */
    public function editAction($id) 
    {
        if (is_null($id)) {
            $id = $this->getUser()->getId();
        }
        $dm = $this->getDocumentManager();
        
        $document = $dm->getRepository($this->getCurrentBundleName() . ':' . ucfirst($this->documentName))->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find User document.');
        }

        $editForm = $this->createForm(new $this->documentType, $document);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('WildCatsAdminBundle:Crud:edit.html.twig', 
            array(
                'action' => 'edit',
                'document_name' => strtolower($this->documentName),
                'document' => $document,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    public function updateAction(Request $request, $id) 
    {

        $dm = $this->getDocumentManager();
        $document = $dm->getRepository($this->getCurrentBundleName() . ':' . ucfirst($this->documentName))->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find User document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new $this->documentType, $document);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_' . $this->documentName . '_edit', array('id' => $id)));
        }

        return $this->render('WildCatsAdminBundle:Crud:edit.html.twig', 
            array(
                'action' => 'edit',
                'document_name' => strtolower($this->documentName),
                'document' => $document,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }

    /**
     * Deletes a User document.
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository($this->getCurrentBundleName() . ':' . ucfirst($this->documentName))->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find User document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('user'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager() {

        return $this->get('doctrine.odm.mongodb.document_manager');
    }

    /**
     * Returns the CurrentBundleNamme
     *
     * @return DocumentManager
     */
    private function getCurrentBundleName() {

        $classnameArray = explode('\\', get_class($this));

        return $classnameArray[0] . $classnameArray[1];
    }

    private function getBundlePath() {

        $classnameArray = explode('\\', get_class($this));

        return $classnameArray[0] . '\\' . $classnameArray[1];
    }
}