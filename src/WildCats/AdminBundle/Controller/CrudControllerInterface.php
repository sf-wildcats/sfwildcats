<?php

namespace WildCats\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

interface CrudControllerInterface {

	public function indexAction();

	public function showAction($id);

	public function newAction();

	public function createAction(Request $request);

	public function editAction();

	public function updateAction(Request $request, $id);

	public function deleteAction(Request $request, $id);

	private function createDeleteForm($id);

	private function getDocumentManager();

	private function getCurrentBundleName();

	private function getBundlePath();
}