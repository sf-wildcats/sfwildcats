<?php

namespace WildCats\TeamBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use WildCats\TeamBundle\Document\Team;
use WildCats\TeamBundle\Form\TeamType;

/**
 * Team controller.
 */
class AdminTeamController extends Controller
{
    /**
     * Lists all Team documents.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository('WildCatsTeamBundle:Team')->findAll();

        return $this->render('WildCatsTeamBundle:Team:index.html.twig', array('documents' => $documents));
    }

    /**
     * Displays a form to create a new Team document.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $document = new Team();
        $form = $this->createForm(new TeamType(), $document);

        return $this->render('WildCatsTeamBundle:Team:new.html.twig', array(
            'document' => $document,
            'form'     => $form->createView()
        ));
    }

    /**
     * Creates a new Team document.
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(Request $request)
    {
        $document = new Team();
        $form     = $this->createForm(new TeamType(), $document);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_team_show', array('id' => $document->getId())));
        }

        return $this->render('WildCatsTeamBundle:Team:new.html.twig', array(
            'document' => $document,
            'form'     => $form->createView()
        ));
    }

    /**
     * Finds and displays a Team document.
     *
     * @param string $id The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function showAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('WildCatsTeamBundle:Team')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Team document.');
        }

        $deleteForm = $this->createDeleteForm($id);


        return $this->render('WildCatsTeamBundle:Team:show.html.twig', array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Team document.
     *
     * @param string $id The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('WildCatsTeamBundle:Team')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Team document.');
        }

        $editForm = $this->createForm(new TeamType(), $document);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('WildCatsTeamBundle:Team:edit.html.twig', array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Team document.
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('WildCatsTeamBundle:Team')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Team document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new TeamType(), $document);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_team_edit', array('id' => $id)));
        }

        return $this->render('WildCatsTeamBundle:Team:edit.html.twig', array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Team document.
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository('WildCatsTeamBundle:Team')->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find Team document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('admin_team'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
