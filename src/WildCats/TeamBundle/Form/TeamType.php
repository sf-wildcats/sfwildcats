<?php

namespace WildCats\TeamBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TeamType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('logo', 'file', array(
                    'data_class' => 'Doctrine\MongoDB\GridFSFile'
                ))
            ->add('responsables')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'WildCats\TeamBundle\Document\Team'
        ));
    }

    public function getName()
    {
        return 'wildcats_teambundle_teamtype';
    }
}
