<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\TeamBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\TeamBundle\Repository\StatsRepository")
 */
class Stats
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\Member")
     */
    protected $member;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\StatsType")
     */
    protected $type;

    /**
     * @MongoDB\String
     */
    protected $value;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\EventBundle\Document\Match")
     */
    protected $match;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set member
     *
     * @param WildCats\UserBundle\Document\Member $member
     * @return self
     */
    public function setMember(\WildCats\TeamBundle\Document\Member $member)
    {
        $this->member = $member;
        return $this;
    }

    /**
     * Get member
     *
     * @return WildCats\TeamBundle\Document\Member $member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set type
     *
     * @param WildCats\TeamBundle\Document\StatsType $type
     * @return self
     */
    public function setType(\WildCats\TeamBundle\Document\StatsType $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * Get type
     *
     * @return WildCats\TeamBundle\Document\StatsType $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * Get value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set match
     *
     * @param WildCats\EventBundle\Document\Match $match
     * @return self
     */
    public function setMatch(\WildCats\EventBundle\Document\Match $match)
    {
        $this->match = $match;
        return $this;
    }

    /**
     * Get match
     *
     * @return WildCats\EventBundle\Document\Match $match
     */
    public function getMatch()
    {
        return $this->match;
    }
}
