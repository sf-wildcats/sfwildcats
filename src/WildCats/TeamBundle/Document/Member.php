<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\TeamBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\TeamBundle\Repository\MemberRepository")
 */
class Member
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\UserBundle\Document\User")
     */
    protected $user;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\SectionMember")
     */
    protected $sectionrattachments;

    /**
     * @MongoDB\String
     */
    protected $licencenumber;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param WildCats\UserBundle\Document\User $user
     * @return self
     */
    public function setUser(\WildCats\UserBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return WildCats\UserBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set sectionrattachments
     *
     * @param WildCats\TeamBundle\Document\SectionMember $sectionrattachments
     * @return self
     */
    public function setSectionrattachments(\WildCats\TeamBundle\Document\SectionMember $sectionrattachments)
    {
        $this->sectionrattachments = $sectionrattachments;
        return $this;
    }

    /**
     * Get sectionrattachments
     *
     * @return WildCats\TeamBundle\Document\SectionMember $sectionrattachments
     */
    public function getSectionrattachments()
    {
        return $this->sectionrattachments;
    }

    /**
     * Set licencenumber
     *
     * @param string $licencenumber
     * @return self
     */
    public function setLicencenumber($licencenumber)
    {
        $this->licencenumber = $licencenumber;
        return $this;
    }

    /**
     * Get licencenumber
     *
     * @return string $licencenumber
     */
    public function getLicencenumber()
    {
        return $this->licencenumber;
    }
}
