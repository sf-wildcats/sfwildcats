<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\TeamBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\TeamBundle\Repository\SectionMemberRepository")
 */
class SectionMember
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\Member")
     */
    protected $member;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\Section")
     */
    protected $section;

    /**
     * @MongoDB\String
     */
    protected $defaultposition;

    /**
     * @MongoDB\String
     */
    protected $positionwanted;

    /**
     * @MongoDB\ReferenceMany(targetDocument="WildCats\TeamBundle\Document\Stats")
     */
    protected $stats;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->stats = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add member
     *
     * @param WildCats\UserBundle\Document\Member $member
     */
    public function addMember(\WildCats\TeamBundle\Document\Member $member)
    {
        $this->members[] = $member;
    }

    /**
     * Remove member
     *
     * @param WildCats\UserBundle\Document\Member $member
     */
    public function removeMember(\WildCats\TeamBundle\Document\Member $member)
    {
        $this->members->removeElement($member);
    }

    /**
     * Get members
     *
     * @return Doctrine\Common\Collections\Collection $members
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Add section
     *
     * @param WildCats\TeamBundle\Document\Section $section
     */
    public function addSection(\WildCats\TeamBundle\Document\Section $section)
    {
        $this->sections[] = $section;
    }

    /**
     * Remove section
     *
     * @param WildCats\TeamBundle\Document\Section $section
     */
    public function removeSection(\WildCats\TeamBundle\Document\Section $section)
    {
        $this->sections->removeElement($section);
    }

    /**
     * Get sections
     *
     * @return Doctrine\Common\Collections\Collection $sections
     */
    public function getSections()
    {
        return $this->sections;
    }

    /**
     * Set defaultposition
     *
     * @param string $defaultposition
     * @return self
     */
    public function setDefaultposition($defaultposition)
    {
        $this->defaultposition = $defaultposition;
        return $this;
    }

    /**
     * Get defaultposition
     *
     * @return string $defaultposition
     */
    public function getDefaultposition()
    {
        return $this->defaultposition;
    }

    /**
     * Set positionwanted
     *
     * @param string $positionwanted
     * @return self
     */
    public function setPositionwanted($positionwanted)
    {
        $this->positionwanted = $positionwanted;
        return $this;
    }

    /**
     * Get positionwanted
     *
     * @return string $positionwanted
     */
    public function getPositionwanted()
    {
        return $this->positionwanted;
    }

    /**
     * Add stat
     *
     * @param WildCats\TeamBundle\Document\Stats $stat
     */
    public function addStat(\WildCats\TeamBundle\Document\Stats $stat)
    {
        $this->stats[] = $stat;
    }

    /**
     * Remove stat
     *
     * @param WildCats\TeamBundle\Document\Stats $stat
     */
    public function removeStat(\WildCats\TeamBundle\Document\Stats $stat)
    {
        $this->stats->removeElement($stat);
    }

    /**
     * Get stats
     *
     * @return Doctrine\Common\Collections\Collection $stats
     */
    public function getStats()
    {
        return $this->stats;
    }

    /**
     * Set member
     *
     * @param WildCats\TeamBundle\Document\Member $member
     * @return self
     */
    public function setMember(\WildCats\TeamBundle\Document\Member $member)
    {
        $this->member = $member;
        return $this;
    }

    /**
     * Get member
     *
     * @return WildCats\TeamBundle\Document\Member $member
     */
    public function getMember()
    {
        return $this->member;
    }

    /**
     * Set section
     *
     * @param WildCats\TeamBundle\Document\Section $section
     * @return self
     */
    public function setSection(\WildCats\TeamBundle\Document\Section $section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Get section
     *
     * @return WildCats\TeamBundle\Document\Section $section
     */
    public function getSection()
    {
        return $this->section;
    }
}
