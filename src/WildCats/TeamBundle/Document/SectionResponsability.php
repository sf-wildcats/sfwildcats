<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\TeamBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\TeamBundle\Repository\SectionResponsabilityRepository")
 */
class SectionResponsability
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\UserBundle\Document\User")
     */
    protected $user;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\Section")
     */
    protected $section;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set user
     *
     * @param WildCats\UserBundle\Document\User $user
     * @return self
     */
    public function setUser(\WildCats\UserBundle\Document\User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return WildCats\UserBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set section
     *
     * @param WildCats\TeamBundle\Document\Section $section
     * @return self
     */
    public function setSection(\WildCats\TeamBundle\Document\Section $section)
    {
        $this->section = $section;
        return $this;
    }

    /**
     * Get section
     *
     * @return WildCats\TeamBundle\Document\Section $section
     */
    public function getSection()
    {
        return $this->section;
    }
}
