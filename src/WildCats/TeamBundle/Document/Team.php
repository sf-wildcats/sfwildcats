<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\TeamBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\TeamBundle\Repository\TeamRepository")
 */
class Team
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\File
     */
    protected $logo;

    /**
     * @MongoDB\ReferenceMany(targetDocument="WildCats\TeamBundle\Document\TeamResponsability")
     */
    protected $responsables;

    public function __construct()
    {
        $this->responsables = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set logo
     *
     * @param file $logo
     * @return self
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }

    /**
     * Get logo
     *
     * @return file $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Add responsable
     *
     * @param WildCats\TeamBundle\Document\TeamResponsability $responsable
     */
    public function addResponsable(\WildCats\TeamBundle\Document\TeamResponsability $responsable)
    {
        $this->responsables[] = $responsable;
    }

    /**
     * Remove responsable
     *
     * @param WildCats\TeamBundle\Document\TeamResponsability $responsable
     */
    public function removeResponsable(\WildCats\TeamBundle\Document\TeamResponsability $responsable)
    {
        $this->responsables->removeElement($responsable);
    }

    /**
     * Get responsables
     *
     * @return Doctrine\Common\Collections\Collection $responsables
     */
    public function getResponsables()
    {
        return $this->responsables;
    }
}
