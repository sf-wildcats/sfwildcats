<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\TeamBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\TeamBundle\Repository\SectionRepository")
 */
class Section
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\UserBundle\Document\User")
     */
    protected $name;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\TeamBundle\Document\team")
     */
    protected $team;

    /**
     * @MongoDB\String
     */
    protected $description;

    /**
     * @MongoDB\Int
     */
    protected $age;

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param WildCats\UserBundle\Document\User $name
     * @return self
     */
    public function setName(\WildCats\UserBundle\Document\User $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return WildCats\UserBundle\Document\User $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set team
     *
     * @param WildCats\TeamBundle\Document\team $team
     * @return self
     */
    public function setTeam(\WildCats\TeamBundle\Document\team $team)
    {
        $this->team = $team;
        return $this;
    }

    /**
     * Get team
     *
     * @return WildCats\TeamBundle\Document\team $team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return self
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set age
     *
     * @param int $age
     * @return self
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * Get age
     *
     * @return int $age
     */
    public function getAge()
    {
        return $this->age;
    }
}
