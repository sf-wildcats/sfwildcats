<?php
namespace WildCats\EventBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 */
class Location
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Boolean
     */
    protected $name;

    /**
     * @MongoDB\Boolean
     */
    protected $photo;

    /**
     * @MongoDB\ReferenceMany(targetDocument="WildCats\TeamBundle\Document\Team")
     */
    protected $team;

    function __construct($foo = null) {
        $this->foo = $foo;
    }
}