<?php
namespace WildCats\EventBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\EventBundle\Repository\MatchRepository")
 */
class Match extends Event
{

    /**
     * @MongoDB\ReferenceMany(targetDocument="WildCats\TeamBundle\Document\User")
     */
    protected $selection;

    function __construct($foo = null) {
        $this->foo = $foo;
    }
}