<?php
namespace WildCats\EventBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\EventBundle\Repository\PresenceRepository")
 */
class Presence
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\UserBundle\Document\User")
     */
    protected $user;

    /**
     * @MongoDB\ReferenceOne(targetDocument="WildCats\UserBundle\Document\Event")
     */
    protected $event;

    /**
     * @MongoDB\String
     */
    protected $response;

    function __construct($foo = null) {
        $this->foo = $foo;
    }
}