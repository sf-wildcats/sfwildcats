<?php
namespace WildCats\EventBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\EventBundle\Repository\MatchRepository")
 */
class Training extends Event
{

    /**
     * @MongoDB\Boolean
     */
    protected $isrecurrence;
}