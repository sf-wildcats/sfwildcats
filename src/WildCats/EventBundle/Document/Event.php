<?php
namespace WildCats\EventBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document(repositoryClass="WildCats\EventBundle\Repository\EventRepository")
 * @MongoDB\InheritanceType("SINGLE_COLLECTION")
 * @MongoDB\DiscriminatorField("type")
 * @MongoDB\DiscriminatorMap({"event"="Event", "match"="Match", "training"="Training"})
 */
class Event
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\Boolean
     */
    protected $isallday;

    /**
     * @MongoDB\Date
     */
    protected $begindate; 

    /**
     * @MongoDB\Date
     */
    protected $enddate; 

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\String
     */
    protected $description;

    /**
     * @MongoDB\String
     */
    protected $commentaire;

    /**
     * @MongoDB\ReferenceOne(targetDocument="Location")
     */
    protected $location;

    /**
     * @MongoDB\ReferenceMany(targetDocument="WildCats\UserBundle\Document\User")
     */
    protected $responsables;

    /**
     * MongoDB\ReferenceMany(targetDocument="WildCats\UserBundle\Document\Comment")
     */
    protected $comments;

    function __construct($foo = null) {
        $this->foo = $foo;
    }
}