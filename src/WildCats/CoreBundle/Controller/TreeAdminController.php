<?php

namespace WildCats\CoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WildCats\CoreBundle\Document\Tree;
use WildCats\CoreBundle\Form\TreeType;

/**
 * Tree controller.
 *
 * @Route("/admin/tree")
 */
class TreeAdminController extends Controller
{
    /**
     * Lists all Tree documents.
     *
     * @Route("/", name="admin_tree")
     * @Template()
     *
     * @return array
     */
    public function indexAction()
    {
        $dm = $this->getDocumentManager();

        $documents = $dm->getRepository('WildCatsCoreBundle:Tree')->findAll();

        return array('documents' => $documents);
    }

    /**
     * Displays a form to create a new Tree document.
     *
     * @Route("/new", name="admin_tree_new")
     * @Template()
     *
     * @return array
     */
    public function newAction()
    {
        $document = new Tree();
        $form = $this->createForm(new TreeType(), $document);

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Creates a new Tree document.
     *
     * @Route("/create", name="admin_tree_create")
     * @Method("POST")
     * @Template("WildCatsCoreBundle:Tree:new.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function createAction(Request $request)
    {
        $document = new Tree();
        $form     = $this->createForm(new TreeType(), $document);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_tree_edit', array('id' => $document->getId())));
        }

        return array(
            'document' => $document,
            'form'     => $form->createView()
        );
    }

    /**
     * Finds and displays a Tree document.
     *
     * @Route("/{id}/show", name="admin_tree_edit")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function showAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('WildCatsCoreBundle:Tree')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Tree document.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document' => $document,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Tree document.
     *
     * @Route("/{id}/edit", name="admin_tree_edit")
     * @Template()
     *
     * @param string $id The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function editAction($id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('WildCatsCoreBundle:Tree')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Tree document.');
        }

        $editForm = $this->createForm(new TreeType(), $document);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Tree document.
     *
     * @Route("/{id}/update", name="admin_tree_update")
     * @Method("POST")
     * @Template("WildCatsCoreBundle:Tree:edit.html.twig")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function updateAction(Request $request, $id)
    {
        $dm = $this->getDocumentManager();

        $document = $dm->getRepository('WildCatsCoreBundle:Tree')->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find Tree document.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm   = $this->createForm(new TreeType(), $document);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $dm->persist($document);
            $dm->flush();

            return $this->redirect($this->generateUrl('admin_tree_edit', array('id' => $id)));
        }

        return array(
            'document'    => $document,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Tree document.
     *
     * @Route("/{id}/delete", name="admin_tree_delete")
     * @Method("POST")
     *
     * @param Request $request The request object
     * @param string $id       The document ID
     *
     * @return array
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException If document doesn't exists
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $dm = $this->getDocumentManager();
            $document = $dm->getRepository('WildCatsCoreBundle:Tree')->find($id);

            if (!$document) {
                throw $this->createNotFoundException('Unable to find Tree document.');
            }

            $dm->remove($document);
            $dm->flush();
        }

        return $this->redirect($this->generateUrl('admin_tree'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    /**
     * Returns the DocumentManager
     *
     * @return DocumentManager
     */
    private function getDocumentManager()
    {
        return $this->get('doctrine.odm.mongodb.document_manager');
    }
}
