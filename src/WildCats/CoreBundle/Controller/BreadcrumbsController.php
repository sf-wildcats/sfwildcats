<?php

namespace WildCats\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

class BreadcrumbsController extends Controller
{
    /**
     * @Route("")
     * @Template()
     */
    public function renderBreadcrumbAction()
    {
        $request = $this->container->get('request');
        $routeName = $request->attributes->get('_route');
        //var_dump($routeName);
        $content = $this->renderView(
            'WildCatsCoreBundle:Breadcrumbs:breadcrumbs.html.twig',
            array('routename' => $routeName)
        );
        
        return new Response($content);
    }
}
