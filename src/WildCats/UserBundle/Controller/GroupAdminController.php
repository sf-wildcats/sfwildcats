<?php

namespace WildCats\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WildCats\UserBundle\Document\Group;
use WildCats\UserBundle\Form\GroupType;
use WildCats\AdminBundle\Controller\CrudControllerTrait;
use WildCats\AdminBundle\Controller\CrudControllerInterface;

class GroupAdminController extends Controller {
        use CrudControllerTrait;
    
    protected $documentName = 'Group';
    protected $documentClass = 'WildCats\UserBundle\Document\Group';
    protected $documentType = 'WildCats\UserBundle\Form\GroupType';
    
    public function setListFields() {

        $this->listFields[] = 'name';
        $this->listFields[] = 'created';
        $this->listFields[] = 'updated';

        return $this->listFields;
    }
}
