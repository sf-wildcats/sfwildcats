<?php

namespace WildCats\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use WildCats\UserBundle\Document\User;
use WildCats\UserBundle\Form\UserType;
use WildCats\AdminBundle\Controller\CrudControllerTrait;
use WildCats\AdminBundle\Controller\CrudControllerInterface;

class UserAdminController extends Controller
{
    use CrudControllerTrait;
    
    protected $documentName = 'User';
    protected $documentClass = 'WildCats\UserBundle\Document\User';
    protected $documentType = 'WildCats\UserBundle\Form\UserType';
    
    public function setListFields() {

        $this->listFields[] = 'firstname';
        $this->listFields[] = 'lastname';
        $this->listFields[] = 'created';
        $this->listFields[] = 'updated';


        return $this->listFields;
    }

    /**
     * Displays a Form for edit a document.
     *
     * @return array
     */
    public function meAction($id = null) 
    {
        if (is_null($id)) {
            $id = $this->getUser()->getId();
        }
        $dm = $this->getDocumentManager();
        
        $document = $dm->getRepository($this->getCurrentBundleName() . ':' . ucfirst($this->documentName))->find($id);

        if (!$document) {
            throw $this->createNotFoundException('Unable to find User document.');
        }

        $editForm = $this->createForm(new $this->documentType, $document);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('WildCatsAdminBundle:Crud:edit.html.twig', 
            array(
                'action' => 'edit',
                'document_name' => strtolower($this->documentName),
                'document' => $document,
                'edit_form'   => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            )
        );
    }
}
