<?php

namespace WildCats\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PermissionAdminController extends Controller {
    use CrudControllerTrait;
    
    protected $documentName = 'Permission';
    protected $documentClass = 'WildCats\UserBundle\Document\Permission';
    protected $documentType = 'WildCats\UserBundle\Form\PermissionType';
    
    public function setListFields() {

        $this->listFields[] = 'name';

        return $this->listFields;
    }
}
