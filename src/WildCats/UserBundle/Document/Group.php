<?php
// src/WildCats/UserBundle/Document/Group.php

namespace WildCats\UserBundle\Document;

use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @MongoDB\Document(repositoryClass="WildCats\UserBundle\Repository\GroupRepository")
 */
class Group extends BaseGroup
{
    /**
     * @MongoDB\Id(strategy="auto")
     */
    protected $id;

    /**
     * @MongoDB\String
     */
    protected $name;

    /**
     * @MongoDB\String
     * @MongoDB\UniqueIndex
     * @Gedmo\Slug(fields={"name"})
     */
    protected $slug;

    /**
     * @MongoDB\ReferenceMany(targetDocument="Permissions", strategy="addToSet")
     */
    protected $permissions = array();

    /**
     * @MongoDB\ReferenceMany(targetDocument="User", strategy="addToSet")
     */
    private $users = array();

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }
    public function __construct()
    {
        $this->permissions = new \Doctrine\Common\Collections\ArrayCollection();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Set name
     *
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return self
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * Get slug
     *
     * @return string $slug
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add permission
     *
     * @param WildCats\UserBundle\Document\Permissions $permission
     */
    public function addPermission(\WildCats\UserBundle\Document\Permission $permission)
    {
        $this->permissions[] = $permission;
    }

    /**
     * Remove permission
     *
     * @param WildCats\UserBundle\Document\Permissions $permission
     */
    public function removePermission(\WildCats\UserBundle\Document\Permission $permission)
    {
        $this->permissions->removeElement($permission);
    }

    /**
     * Get permissions
     *
     * @return Doctrine\Common\Collections\Collection $permissions
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * Add user
     *
     * @param WildCats\UserBundle\Document\User $user
     */
    public function addUser(\WildCats\UserBundle\Document\User $user)
    {
        $this->users[] = $user;
    }

    /**
     * Remove user
     *
     * @param WildCats\UserBundle\Document\User $user
     */
    public function removeUser(\WildCats\UserBundle\Document\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return Doctrine\Common\Collections\Collection $users
     */
    public function getUsers()
    {
        return $this->users;
    }
}
